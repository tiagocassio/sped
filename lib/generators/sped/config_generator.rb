# frozen_string_literal: true
module Sped
    module Generators
      # rails g kaminari:config
      class ConfigGenerator < Rails::Generators::Base # :nodoc:
        source_root File.expand_path(File.join(File.dirname(__FILE__), 'templates'))
  
        desc <<DESC
  Description:
      Copies Sped configuration file to your application's initializer directory.
  DESC
        def copy_config_file
          template 'sped_config.rb', 'config/initializers/sped_config.rb'
        end
      end
    end
  end